<?php
    require_once './Twig/Autoloader.php';
    Twig_Autoloader::register();
    $loader = new Twig_Loader_Filesystem('./.templates');
    $twig = new Twig_Environment($loader);

    $list_ignore = array ('.','..','.git','Workspace','Dashboard');

    $project=array();
    $image=array();
    $readme=array();
    $i=0;
    $handle=opendir('..');
    while ($file = readdir($handle)) {
        $file_path=$_SERVER['DOCUMENT_ROOT'].'/'.$file;
        if (is_dir($file_path) && !in_array($file,$list_ignore)) {
            $project[$i]=$file;

            $image_path=$file_path.'/img/dashboard-img.jpg';
            if(file_exists($image_path)){
                $image[$i]='../'.$project[$i].'/img/dashboard-img.jpg';
            }else{
                $image[$i]='img/default.jpg';
            }

            $i++;
            $file_path_readme=$_SERVER['DOCUMENT_ROOT'].'/'.$file.'/README.txt';
            if(file_exists($file_path_readme)){
                $handle_readme = fopen($file_path_readme,'r');
                if(filesize($file_path_readme)) {
                    $readme[$i] = fread($handle_readme, filesize($file_path_readme));
                    $readme[$i] = nl2br($readme[$i], true);
                }
                    fclose($handle_readme);
            }
            $file_path_readme=$_SERVER['DOCUMENT_ROOT'].'/'.$file.'/README.md';
            if(file_exists($file_path_readme)){
                $handle_readme = fopen($file_path_readme,'r');
                if(filesize($file_path_readme)) {
                    $readme[$i] = fread($handle_readme, filesize($file_path_readme));
                    $readme[$i] = nl2br($readme[$i], true);
                }
                    fclose($handle_readme);
            }
            else{
                $readme[$i]='No description available';
            }
        }
    }
    closedir($handle);

    $template = $twig->loadTemplate('index.html');
    echo $twig->render('index.html', array('projects' => $project, 'readme' => $readme,'images' => $image));
?>