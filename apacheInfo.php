<?php
    require_once './Twig/Autoloader.php';
    Twig_Autoloader::register();
    $loader = new Twig_Loader_Filesystem('./.templates');
    $twig = new Twig_Environment($loader);

    // Version d'apache.
     
    $apache_version = explode('PHP', apache_get_version());
    $php_version = phpversion();
    $loaded_extensions = get_loaded_extensions();
    

    $template = $twig->loadTemplate('apacheInfo.html');
    echo $twig->render('apacheInfo.html', array('apache_version' => $apache_version, 
                                                'php_version' => $php_version,
                                                'loaded_extensions' => $loaded_extensions));
?>